package net.iescierva.dam18_26.p0401automata;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText inputValuesUI;
    private Automata automata;
    private TextView result;
    private List<Integer> posValid;
    private int posStateCurrent;
    private int[][] arrayMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        initValues();
        automata = new Automata(posValid,posStateCurrent,arrayMap);
        inputValuesUI = findViewById(R.id.editExpresion);
        result = findViewById(R.id.txtValidado);
    }

    public void validarDatos(View view){
        char[] datos = inputValuesUI.getText().toString().toCharArray();

        if (automata.isValid(datos))
            result.setText(R.string.aceptado);
        else
            result.setText(R.string.rechazado_result);
    }


    private void initValues() {
        arrayMap = new int[][]{{1, 2, -1, 3},
                                {1, 2, -1, 3},
                                {-1, 2, 4, 3},
                                {-1, 3, 4, -1},
                                {5, 6, -1, -1},
                                {5, 6, -1, -1},
                                {-1, 6, -1, -1}};
        posValid = Arrays.asList(2,3,6);
        posStateCurrent = 0;
    }

}
