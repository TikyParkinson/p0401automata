package net.iescierva.dam18_26.p0401automata;

import java.util.List;

public class Automata {

    private List<Integer> posValid;
    private int posStateCurrent;
    private int[][] arrayMap;
    private int posStart;


    public Automata(List<Integer> posValid, int posStateCurrent, int[][] arrayMap) {
        this.posStateCurrent = posStateCurrent;
        this.posValid = posValid;
        this.arrayMap = arrayMap;
    }

    public boolean isValid(char[] datos) {

        posStart = posStateCurrent;

        for (char caracter: datos) {

            switch (caracter) {
                case '-': case '+':
                    posStateCurrent = 0;
                    break;
                case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
                    posStateCurrent = 1;
                    break;
                case 'e': case 'E':
                    posStateCurrent = 2;
                    break;
                case '.': case ',':
                    posStateCurrent = 3;
                    break;
                default:
                    return false;
            }

            posStart = arrayMap[posStart][posStateCurrent];

            if (posStart == -1) {
                return false;
            }

        }
        return posValid.contains(posStart);
    }
}
